#!/usr/bin/env python
# -*- coding: utf-8 -*-
# (C) Guilhem Marion 2019 - rename_ydl.py
# This file copies/moves files downloaded with youtube-dl mainly to remove
# the last part in their names (which is Youtube's ID for the corresponding
# video).

from argparse import ArgumentParser
import os
import shutil

def main():
    parser = ArgumentParser()
    parser.add_argument(
        "directory",
        default="./",
        help="Directory where files are located"
    )
    parser.add_argument(
        "--rename",
        help="Rename files (default) instead of copying them",
        default="True",
        action="store_true"
    )
    parser.add_argument(
        "--copy",
        help="Do not rename files, copying them instead",
        dest="rename",
        action="store_false"
    )
    args = parser.parse_args()

    # Perform usual check on target directory
    if not os.path.isdir(args.directory):
        print("Invalid directory '{}' !".format(args.directory))
        exit(-1)
    else:
        rename_files(args.directory, args.rename)


def rename_files(target_dir, rename=True):
    for filename in os.listdir(target_dir):
        # Split file name
        file_split = filename.split('-')

        # Build new name
        # TODO: Make extension configurable
        # TODO: If we do not match, do not copy/replace
        # TODO: Maybe, if the required extensions are not matched, pass ?
        new_name = "%s-%s.mp3" % (file_split[0], file_split[1])

        # Rename the file
        fulfname = "/".join([target_dir, filename])
        fulnname = "/".join([target_dir, new_name])
        if rename:
            print "Moving {} to {}".format(fulfname, fulnname)
            os.rename(fulfname, fulnname)
        else:
            print "Copying {} to {}".format(fulfname, fulnname)
            shutil.copyfile(fulfname, fulnname)

if __name__ == '__main__':
    main()
